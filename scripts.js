var txt_minusculas = "abcdefghijklmnopqrstuvwxyz";
var txt_mayusculas = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var txt_numeros = "0123456789";
var txt_simbolos = "#$@!%&*?";
var txt_especiales = "¡\"/\+,.:;='^|~-_()¿{}[]";

var input_password = document.getElementById("input_password");
var input_tamano = document.getElementById("input_tamano");
var input_minusculas = document.getElementById("input_minusculas");
var input_mayusculas = document.getElementById("input_mayusculas");
var input_numeros = document.getElementById("input_numeros");
var input_caracteres_simbolos = document.getElementById(
  "input_caracteres_simbolos"
);
var input_caracteres_especiales = document.getElementById(
  "input_caracteres_especiales"
);
var input_generar = document.querySelector("#generar button");

function randomIndex(number) {
  return Math.floor(Math.random(number) * number);
}

function getCaracteres(caracteres, limit = 1) {
  if (limit === 1) {
    var index = randomIndex(caracteres.length);
    return caracteres[index];
  }
  var password = "";
  for (i = 0; i < limit; i++) {
    var index = randomIndex(caracteres.length);
    password += caracteres[index];
  }
  return password;
}

function randomize(string) {
  var newString = "";
  var string_arr = string.split("");
  var count = string_arr.length;
  for (i = 0; i < count; i++) {
    var index = 0;
    if (string_arr.length > 1) {
      index = randomIndex(string_arr.length);
    }
    newString += string_arr.splice(index, 1);
  }
  return newString;
}

function generar() {
  var password = "";
  var tamano = input_tamano.value;
  var minusculas = input_minusculas.checked;
  var mayusculas = input_mayusculas.checked;
  var numeros = input_numeros.checked;
  var caracteres_simbolos = input_caracteres_simbolos.checked;
  var caracteres_especiales = input_caracteres_especiales.checked;

  if (isNaN(tamano)) {
    tamano = 1;
  }
  if (tamano > 256) {
    tamano = 256;
    input_tamano.value = 256;
  }

  var caracteres = "";

  if (!minusculas && !mayusculas && !numeros && !caracteres_especiales) {
    input_minusculas.checked = true;
    caracteres = txt_minusculas;
  }

  if (minusculas) {
    caracteres += txt_minusculas;
    password += getCaracteres(txt_minusculas);
  }
  if (mayusculas) {
    caracteres += txt_mayusculas;
    password += getCaracteres(txt_mayusculas);
  }
  if (numeros) {
    caracteres += txt_numeros;
    password += getCaracteres(txt_numeros);
  }
  if (caracteres_especiales) {
    caracteres += txt_especiales;
    password += getCaracteres(txt_especiales);
  }
  if (caracteres_simbolos) {
    caracteres += txt_simbolos;
    password += getCaracteres(txt_simbolos);
  }

  // TODO: AQUI los caracteres faltantes
  password += getCaracteres(caracteres, tamano - password.length);

  input_password.value = randomize(password);
}

input_generar.addEventListener("click", () => {
  generar();
});

input_tamano.addEventListener("input", () => {
  generar();
});

input_minusculas.addEventListener("change", () => {
  generar();
});
input_mayusculas.addEventListener("change", () => {
  generar();
});
input_numeros.addEventListener("change", () => {
  generar();
});
input_caracteres_especiales.addEventListener("change", () => {
  generar();
});

input_password.addEventListener("click", () => {
  input_password.select();
  document.execCommand("copy");
});

window.addEventListener("load", () => {
  generar();
});
